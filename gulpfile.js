var gulp = require('gulp');
var sass = require('gulp-sass');
var browserSync = require('browser-sync');

gulp.task('animation', function(){
	return gulp.src('front/sass/**/*.scss')
	.pipe(sass().on('error', sass.logError))
	.pipe(gulp.dest('front/'))
	.pipe(browserSync.reload({stream: true}))
});

gulp.task('browser-sync', function(){
	browserSync({
		server: {
			baseDir: 'front'
		},
		notifty: false
	})
});

gulp.task('watch', ['browser-sync', 'animation'], function(){
	gulp.watch('front/sass/**/*.scss', ['animation']);
	gulp.watch('front/**/*.html', browserSync.reload);
	gulp.watch('front/js/**/*.html', browserSync.reload);
});
